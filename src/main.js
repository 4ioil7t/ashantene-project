import Vue from 'vue'
import App from './App.vue'
import store from './store/index.js'
import VueMeta from 'vue-meta'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'
 
Vue.use(VueAxios, axios)
Vue.use(VueRouter)
Vue.use(VueMeta)
Vue.use(Vuex);

Vue.config.productionTip = false

new Vue({
  el: '#app',
  store: store,
  render: h => h(App),
}).$mount('#app')
